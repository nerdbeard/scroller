use std::{fmt::Debug, time::Duration};

use crate::scroller::Scroller;

/// A step is an action combined with a delay
struct Step {
    /// For debugging
    name: String,
    /// Called with the scroller when the step is executed
    action: Action,
    /// Delay before the following step is executed
    delay: Duration,
}

impl Debug for Step {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        return f.write_fmt(format_args!(
            "[Action: {}, Delay: {}]",
            self.name,
            self.delay.as_secs_f32()
        ));
    }
}

// impl Display for Step {
//     fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
//         return f.write_fmt(format_args!(
//             "[Action: {}, Delay: {}]",
//             self.name,
//             self.delay.as_secs_f32()
//         ));
//     }
// }

#[derive(Default)]
pub struct Script {
    progn: Vec<Step>,
}

impl Script {
    pub fn run(self) {
        let mut remainder = Duration::ZERO;
        let mut scroller: Scroller = Scroller::default();
        for step in self.progn {
            (step.action)(&mut scroller);
            remainder = scroller.scroll(if step.delay == Duration::MAX {
                step.delay
            } else {
                step.delay + remainder
            });
        }
    }

    pub fn add_step(&mut self, name: &str, action: Action, delay: Duration) {
        self.progn.push(Step {
            name: String::from(name),
            action,
            delay,
        })
    }
}

type Action = fn(&mut Scroller);
