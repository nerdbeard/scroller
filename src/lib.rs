mod prog;
mod scroller;

pub use prog::Script;
pub use scroller::{Displacer, Point, Scroller};

#[macro_export]
macro_rules! displacer {
    ($displacer:ident, $body:expr) => {
        const $displacer: Displacer = $body;
    };
    ($action:ident, $displacer:ident, $body:expr, $duration:expr) => {
        displacer!($displacer, $body);
        fn $action(scroller: &mut Scroller) {
            scroller.start_displacer($displacer, $duration);
        }
    };
    ($action:ident, $displacer:ident, $body:expr) => {
        displacer!($action, $displacer, $body, Duration::MAX);
    };
}

/// Take a pair of displacers and mix them according to another
/// function
#[macro_export]
macro_rules! mix {
    ($lhs:expr, $rhs:expr, $factor:expr) => {
        |index, time, started, duration, mut point: &mut Point| {
            let factor_func = $factor;
            let factor = factor_func(index, time, started, duration, &point);
            assert!(
                (0.0..=1.0).contains(&factor),
                "factor must be zero to one, is {factor:?}"
            ); // 1.0 >= factor >= 0.0
            let mut lhp = Point::default();
            let mut rhp = Point::default();
            ($lhs)(index, time, started, duration, &mut lhp);
            ($rhs)(index, time, started, duration, &mut rhp);
            point += lhp * factor + rhp * (1.0 - factor);
        }
    };
    ($script:expr, $duration:expr, $lhs:expr, $rhs:expr, $factor:expr) => {
        add_step!(
            $script,
            |scroller: &mut Scroller| {
                scroller.start_displacer(mix!($lhs, $rhs, $factor), $duration);
            },
            $duration
        )
    };
}

/// Take a pair of displacers and fade linearly from left to right
#[macro_export]
macro_rules! fade {
    ($lhs:expr, $rhs:expr) => {
        mix!($lhs, $rhs, |_,
                          time: Duration,
                          started: Duration,
                          duration: Duration,
                          _|
         -> f32 {
            1.0 - (time - started).as_secs_f32() / duration.as_secs_f32()
        })
    };
    ($script:expr, $duration:expr, $lhs:expr, $rhs:expr) => {
        let action =
            |scroller: &mut Scroller| scroller.start_displacer(fade!($lhs, $rhs), $duration);
        add_step!($script, action, $duration)
    };
}

/// Capture debugging information so steps are printable
#[macro_export]
macro_rules! add_step {
    ($script:expr, $action:expr, $delay:expr) => {
        $script.add_step(stringify!($action), $action, $delay);
    };
}

#[macro_export]
macro_rules! start_displacer {
    ($script:expr, $displacer:expr, $duration:expr, $delay:expr) => {
        add_step!(
            $script,
            |scroller| {
                scroller.start_displacer($displacer, $duration);
            },
            $delay
        );
    };
}

#[macro_export]
macro_rules! wait {
    ($script:expr, $duration:expr) => {
        add_step!($script, |s| {}, $duration);
    };
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::time::Duration;

    displacer!(ZERO, |_, _, _, _, _| {});
    displacer!(ONE, |_, _, _, _, p| {
        p.x = 1.0;
        p.y = 1.0;
    });

    #[test]
    fn mix() {
        let mix = mix!(ONE, ZERO, |_, time: Duration, _, _, _| {
            time.as_secs_f32()
        });
        for step in 0..=10 {
            let t = step as f32 * 0.1;
            let mut p = (10.0, 10.0).into();
            mix(
                0,
                Duration::from_secs_f32(t),
                Duration::ZERO,
                Duration::MAX,
                &mut p,
            );
            // Floating point and base 10, right?
            assert!((t - p.x + 10.0).abs() < 0.001, "t:{t}, p:{p:?}");
            assert!((t - p.y + 10.0).abs() < 0.001, "t:{t}, p:{p:?}");
        }
    }

    #[test]
    fn fade() {
        let fade = fade!(ZERO, ONE);
        for step in 0..=10 {
            let t = step as f32 * 0.1;
            let mut p = (10.0, 10.0).into();
            fade(
                0,
                Duration::from_secs_f32(t),
                Duration::ZERO,
                Duration::from_secs_f32(1.0),
                &mut p,
            );
            // Floating point and base 10, right?
            assert!((t - p.x + 10.0).abs() < 0.001, "t:{t}, p:{p:?}");
            assert!((t - p.y + 10.0).abs() < 0.001, "t:{t}, p:{p:?}");
        }
    }
}
