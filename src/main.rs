//! Demo the scroller library

use std::{f32::consts::PI, time::Duration};

use scroller::{
    add_step, displacer, fade, mix, start_displacer, wait, Displacer, Point, Script, Scroller,
};

// Set up the basic scroller parameters
fn reset(s: &mut Scroller) {
    // Obligatory
    s.set_text(include_str!("scroller.txt"));
    // A non-integer scale makes uniform motion look more interesting,
    // an irrational scale even moreso.
    s.set_scale(PI / 1.5);
    // My screen is 75fps
    s.set_period(Duration::from_millis(1000 / 75));
}

// Called in a loop to make the scale throb
fn throb(s: &mut Scroller) {
    s.set_scale((s.elapsed().as_secs_f32() * PI * 2f32).sin() / 2f32 + 1.75)
}

// Called in a loop to make the scale jump
fn tight(s: &mut Scroller) {
    s.set_scale(PI / 2.0)
}

// Scroll normally
displacer!(smooth, SMOOTH, |index, time, _started, _duration, point| {
    point.x += index as f32 - time.as_secs_f32() * 15.0;
});

// Scroll wildly!
displacer!(wild, WILD, |index, time, _started, _duration, point| {
    let secs = time.as_secs_f32();
    let i = index as f32;
    let scale = (secs / 5.0).sin();
    point.x += (i / 8.0 - secs).sin() * 8.0 * scale;
    point.y += (i / 4.0 + secs * -2.0).sin() * 1.5 + (secs / 4.0).sin() * 2.0 * scale;
});

// Scroll in a simple sine wave
displacer!(
    simple_sin,
    SIMPLE_SIN,
    |index, time, _started, _duration, point| {
        point.y += (index as f32 / 16.0 + time.as_secs_f32() + index as f32 / 5.0).sin() * 4.0
    }
);

// Null displacer for mixing
displacer!(NULL, |_, _, _, _, _| {});

fn main() {
    let mut script = Script::default();

    add_step!(script, reset, Duration::ZERO);
    start_displacer!(script, SMOOTH, Duration::MAX, Duration::from_secs(5));

    for _ in 0..1000 {
        add_step!(script, throb, Duration::from_millis(10));
    }

    wait!(script, Duration::from_secs(3));

    macro_rules! waves {
        () => {
            mix!(SIMPLE_SIN, NULL, |_index,
                                    time: Duration,
                                    _started,
                                    _duration,
                                    _point| {
                (time.as_secs_f32() * 4.0).sin().max(0.0).powf(4.0)
            })
        };
    }

    fade!(script, Duration::from_secs(3), NULL, waves!());
    start_displacer!(
        script,
        waves!(),
        Duration::from_secs(10),
        Duration::from_secs(10)
    );
    fade!(script, Duration::from_secs(3), waves!(), NULL);

    {
        // Pulse like a heartbeat
        let pulse = 80;
        let cycle = 400;
        for _ in 0..8 {
            for _ in 0..2 {
                add_step!(script, tight, Duration::from_millis(pulse));
                add_step!(script, reset, Duration::from_millis(pulse));
                add_step!(script, tight, Duration::from_millis(pulse));
                add_step!(script, reset, Duration::from_millis(cycle));
            }
        }
    }

    fade!(script, Duration::from_secs(10), NULL, SIMPLE_SIN);

    fade!(script, Duration::from_secs(10), SIMPLE_SIN, WILD);

    add_step!(
        script,
        |s| { s.start_displacer(WILD, Duration::from_millis(1000 * 10),) },
        Duration::ZERO
    );

    for _ in 0..1000 {
        add_step!(script, throb, Duration::from_millis(10));
    }

    mix!(
        script,
        Duration::from_secs(120),
        SIMPLE_SIN,
        WILD,
        |index, time: Duration, _, _, _point| {
            (index as f32 + time.as_secs_f32()).sin() / 2.0 + 0.5
        }
    );

    script.run();
}
