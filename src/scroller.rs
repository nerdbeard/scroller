use std::{
    io::{stdout, Write},
    thread::sleep,
    time::{Duration, SystemTime},
};

#[derive(Default, Debug, Copy, Clone, PartialEq)]
pub struct Point {
    pub x: f32,
    pub y: f32,
}

impl std::ops::MulAssign<f32> for Point {
    fn mul_assign(&mut self, rhs: f32) {
        self.x *= rhs;
        self.y *= rhs;
    }
}

impl std::ops::Mul<f32> for Point {
    type Output = Self;
    fn mul(self, rhs: f32) -> Self::Output {
        let mut point = self;
        point *= rhs;
        point
    }
}

impl std::ops::AddAssign<Point> for &mut Point {
    fn add_assign(&mut self, rhs: Point) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

impl std::ops::Add<Point> for Point {
    type Output = Point;
    fn add(self, rhs: Point) -> Self::Output {
        Self::Output {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl From<(f32, f32)> for Point {
    fn from(value: (f32, f32)) -> Self {
        Self {
            x: value.0,
            y: value.1,
        }
    }
}

/// Function for displacement of character over time in
/// [Scroller::scale] units; y=0.0 is the middle of the scroll area
pub type Displacer =
    fn(index: usize, time: Duration, started: Duration, duration: Duration, &mut Point);

pub struct Scroller<'a> {
    /// The entire text that will scroll
    text: &'a str,
    /// Distance between characters where 1.0 is native screen spacing
    scale: f32,
    displacers: Vec<(
        Displacer, // A displacer,
        Duration,  // when it started,
        Duration,  // and how long it runs
    )>,
    /// Maximum updates per second
    period: Duration,
    /// How long the scroller has been run so far
    ticks: u32,
}

impl<'a> Scroller<'a> {
    pub fn set_scale(&mut self, scale: f32) {
        self.scale = scale;
    }

    pub fn set_text(&mut self, text: &'a str) {
        self.text = text;
    }

    pub fn set_period(&mut self, period: Duration) {
        self.period = period;
    }

    pub fn start_displacer(&mut self, displacer: Displacer, duration: Duration) {
        debug_assert!(duration > Duration::ZERO);
        self.displacers
            .push((displacer, self.period * self.ticks, duration));
    }

    /// Takes time to run scroller, returns "unspent" time remaining
    pub fn scroll(&mut self, mut interval: Duration) -> Duration {
        if interval == Duration::ZERO {
            return Duration::ZERO;
        }
        debug_assert!(interval > Duration::ZERO);
        const CLEAR: &[u8] = &[0x1b, 0x5b, 0x48, 0x1b, 0x5b, 0x32, 0x4a];
        let mut stdout = stdout();
        let t0 = SystemTime::now() - self.ticks * self.period;
        let mut buf: String;
        let mut last = String::new();
        while interval > self.period {
            interval -= self.period;
            buf = self.render(SystemTime::now().duration_since(t0).unwrap(), 80, 24);
            if buf != last {
                stdout.write_all(CLEAR).expect("Write error");
                stdout.write_all(buf.as_bytes()).expect("Write error");
                stdout.flush().expect("IO error");
            }
            last = buf;
            self.ticks += 1;
            if let Ok(nap) = (t0 + self.period * self.ticks).duration_since(SystemTime::now()) {
                sleep(nap)
            }
        }
        interval
    }

    pub fn render(&mut self, time: Duration, view_width: usize, view_height: usize) -> String {
        let mut buf = vec![vec![' '; view_width]; view_height];
        let text_width = self.text.len() as f32 * self.scale;
        for (i, char) in self.text.char_indices() {
            let mut point: Point = Point::default();
            let mut expired = vec![];
            for (index, item) in self.displacers.iter().enumerate() {
                let (displacer, started, duration) = item;
                if duration != &Duration::MAX && time - *started > *duration {
                    expired.push(index);
                    continue;
                }
                (*displacer)(i, time, *started, *duration, &mut point);
            }
            // Remove them in reverse order so we don't mess up the
            // indexes we haven't removed yet
            for index in expired.into_iter().rev() {
                self.displacers.remove(index);
            }
            point *= self.scale;
            point.x += (view_width >> 1) as f32;
            point.x %= text_width;
            if point.x < 0.0 {
                // This doesn't necessarily line up and causes some
                // weird artifacts
                point.x += text_width;
            }
            let x = point.x as usize;
            let y = (point.y + (view_height >> 1) as f32) as usize;
            if x < view_width {
                buf[y.min(view_height - 1)][x] = char;
            }
        }

        buf.into_iter()
            .map(|line| format!("{}\n", line.into_iter().collect::<String>()))
            .collect::<String>()
    }

    pub fn elapsed(&self) -> Duration {
        self.ticks * self.period
    }
}

impl Default for Scroller<'_> {
    fn default() -> Self {
        {
            Scroller {
                text: Default::default(),
                scale: 2.5,
                displacers: Default::default(),
                period: Duration::from_millis(100),
                ticks: 0,
            }
        }
    }
}
